const express = require('express');
let { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');
let Producto = require('../models/producto');
const app = express();

// ========================
// Mostrar todos los productos
//=========================

app.get('/', (req, res) => {

    // traer el usuario y la categoria
    // paginado

    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);

    let filtro = { disponible: true };

    Producto.find(filtro)
        .populate('categoria', 'descripcion')
        .populate('usuario', ['nombre', 'email', 'role'])
        .skip(desde)
        .limit(limite)
        .sort('nombre')
        .exec((err, productos) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Producto.countDocuments({ disponible: true }, (err, conteo) => {
                res.json({
                    ok: true,
                    productos,
                    cuantos: conteo
                });
            });
        });
});

// ============================
// Mostrar una producto por ID
//============================

app.get('/:id', (req, res) => {

    let id = req.params.id;

    // traer el usuario y la categoria
    Producto.findById(id)
        .populate('categoria', 'descripcion')
        .populate('usuario', ['nombre', 'email', 'role'])
        .exec((err, producto) => {
            if (err) {
                let error = err;
                return res.status(400).json({
                    ok: false,   
                    err:{
                        message: error.message,
                        name: error.name
                    }
                });
            }

            if (!producto) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: `No se ha encontrado un producto que corresponda al id: ${id}`
                    }
                });
            }

            res.json({
                ok: true,
                producto
            });
        });

});

// =========================
// Crear un nuevo producto
//==========================

app.post('/',[verificaToken], (req, res) => {
    // grabar el usuario
    //grabar la categoria a la que pertenece el producto del listado de categorias

    let body = req.body;

    let producto = new Producto({
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        // disponible: body.disponible,
        categoria: body.categoriaId,
        usuario: req.usuario._id
    });

    producto.save((err, productoDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productoDB) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'No se ha podido guardar el producto'
                }
            });
        }

        res.json({
            ok: true,
            producto: productoDB
        });
    });

});

// =========================
//  Actualizar un producto
//==========================

app.put('/:id',[verificaToken], (req, res) => {
    let body = req.body;
    let id = req.params.id;

    Producto.findByIdAndUpdate(id,
        body,
        { new: true, runValidators: true },
        (err, producto) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                producto
            });
        });
});


//===============================
// Eliminar una producto
//===============================

app.delete('/:id',[verificaToken], (req, res) => {
    // no borrar definitivamente solo colocar disponible en false
    let id = req.params.id;
    let body = req.body;
    Producto.findByIdAndUpdate(id, body, (err, producto) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!producto) {
            return res.status(400).json({
                ok: false,
                err: 'El producto no fue encontrado'
            });
        }

        res.json({
            ok: true,
            message:`El producto con id: ${producto.id} fue eliminado`
        });
    });
});



module.exports = app;