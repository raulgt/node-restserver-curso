// Express me permite crear un servidor web
const express = require('express');
const bcrypt = require('bcrypt');
const _ = require('underscore');
const Usuario = require('../models/usuario');
const app = express();

const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

// Obtenemos el momento exacto de cada request
const requestTime = function (req, res, next) {
    req.requestTime = Date.now()
    next();
}
app.use(requestTime);

//All
app.get('/usuarios', [verificaToken],function (req, res) {

    let filtro = { estado: true };

    Usuario.find(filtro, 'nombre email email2 role', (err, usuarios) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'No se encontraron usuarios..!',
                err
            });
        }


        res.json({
            ok: true,
            usuarios: usuarios
        });
    });
});

app.get('/usuario', [verificaToken], function (req, res) {

    let desde = req.query.desde || 0;

    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);

    //let filtro = {estado:true};

    Usuario.find({}, 'nombre email role estado google img')
        .skip(desde)
        .limit(limite)
        .exec((err, usuarios) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Usuario.countDocuments({estado: true}, (err, conteo) => {
                res.json({
                    ok: true,
                    usuarios,
                    cuantos: conteo
                });
            });
        });
});


app.post('/usuario', [verificaToken, verificaAdmin_Role], function (req, res) {

    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        email2: body.email2,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    });

    usuario.save((err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            usuario: usuarioDB
        });
    });
});

app.put('/usuario/:id',  [verificaToken, verificaAdmin_Role],function (req, res) {
    let id = req.params.id;
    let body = _.omit(req.body, ['google']);

    Usuario.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, usuarioDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            usuario: usuarioDB
        });
    });
});

// elimina el usuario de la base de datos
app.delete('/usuario/:id',  [verificaToken, verificaAdmin_Role], function (req, res) {
    let id = req.params.id;
    Usuario.findByIdAndDelete(id, (err, usuarioBorrado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!usuarioBorrado) {
            return res.status(400).json({
                ok: false,
                err: 'Usuario no encontrado'
            });
        }
        res.json({
            ok: true,
            usuario: usuarioBorrado
        });
    });
});

app.delete('/usuario/softdelete/:id', [verificaToken, verificaAdmin_Role], function (req, res) {
    let id = req.params.id;

    itemsUpdate = {
        estado: false
    }

    Usuario.findByIdAndUpdate(id, itemsUpdate, { new: true }, (err, usuarioBorrado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!usuarioBorrado) {
            return res.status(400).json({
                ok: false,
                err: 'Usuario no encontrado'
            });
        }

        res.json({
            ok: true,
            usuario: usuarioBorrado
        });

    });
});

module.exports = app;