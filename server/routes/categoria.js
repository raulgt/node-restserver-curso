const express = require('express');
const _ = require('underscore');
let { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');
let Categoria = require('../models/categoria');
const app = express();

//===============================
// Mostrar todas las categorias
//===============================

app.get('/', (req, res) => {

    Categoria.find({})
    .sort('descripcion')
    .populate('usuario',['nombre', 'email', 'role'])    
    .exec((err, categorias) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            categorias
        });
    });
});

//===============================
// Mostrar una categoria por ID
//===============================

app.get('/:id', [verificaToken], (req, res) => {

    let id = req.params.id;

    Categoria.findById(id, (err, categoria) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!categoria) {
            return res.status(400).json({
                ok: false,
                err:{
                    message: `No se ha encontrado una categoria que corresponda al id: ${id}`
                }
            });
        }

        res.json({
            ok: true,
            categoria
        });
    });
});


//===============================
// Crear una nueva categoria
//===============================
app.post('/', [verificaToken, verificaAdmin_Role], (req, res) => {
    // regresa nueva categoria
    //req.usuario._id;

    let body = req.body;

    let categoria = new Categoria({
        descripcion: body.descripcion,
        usuario: body.usuario ? body.usuario : req.usuario._id
    });

    categoria.save((err, categoriaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!categoriaDB) {
            return res.status(500).json({
                ok: false,
                err:{
                    message: 'El ID no es correcto'
                }
            });
        }

        res.json({
            ok: true,
            usuario: categoriaDB
        });
    });

});

//===============================
// Actualizar una nueva categoria
//===============================

app.put('/:id', (req, res) => {
    let body = req.body;
    let id = req.params.id;

    Categoria.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, categoria) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            categoria
        });
    });
});

//===============================
// Eliminar una nueva categoria
//===============================

app.delete('/:id', [verificaToken, verificaAdmin_Role],  (req, res) => {
    // solo un administrador puede eliminar la categoria
   let id = req.params.id;
    Categoria.findByIdAndDelete(id, (err, categoriaBorrada) =>{
          
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!categoriaBorrada) {
            return res.status(400).json({
                ok: false,
                err: 'La categoria no fue encontrada'
            });
        }
        res.json({
            ok: true,
            categoriaBorrada
        });
    });
});



module.exports = app;