const express = require('express');
const app = express();

app.use(require('../routes/usuario'));
app.use('/login', require('../routes/login'));
app.use('/categoria', require('../routes/categoria'));
app.use('/producto', require('./producto'));

module.exports = app;