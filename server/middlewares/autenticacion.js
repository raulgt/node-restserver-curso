const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
const jwt = require('jsonwebtoken');
//=====================
// Verificar Token
//=====================

let verificaToken = (req, res, next) => { 
    let token = req.get('token'); // Autorization    

    let decryptToken = cryptr.decrypt(token);   

    jwt.verify(decryptToken, process.env.SEED, (err, decoded) => {   

        if (err) {
            return res.status(401).json({
                ok: false,
                    err
            });
        }
        
        req.usuario = decoded.usuario;
        next();
    });
   
};

let verificaAdmin_Role = (req, res, next)=>{
    let usuario = req.usuario;

    if(usuario.role === 'ADMIN_ROLE' ){       
       next();
    }else{
        return res.status(401).json({
            ok: false,
            err: 'No esta autorizado..!!'
        });
    }
};

module.exports = {
    verificaToken,
    verificaAdmin_Role
}