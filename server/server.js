// Variables globales y de ambiente
require('./config/config');

// Mongoose me permite trabajar con los modelos ademas de la creacion y conexiones de un servidor web
const mongoose = require('mongoose');

// Express me permite interactuar con el servidor web (crear servicios y escuchar conexiones)
const express = require('express');
const app = express();

// bodyParser me permite tratar los datos como objetos json
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json());

// Configuracion global de rutas desde el index.js
app.use(require('./routes/index'));

mongoose.connect(process.env.URLDB, {useNewUrlParser: true}, (err)=>{
         if(err){
            throw err;
         }         
         console.log('Base de datos online');
});

app.listen(process.env.PORT, () => {
  console.log('Escuchando peticiones en el puerto: 3000');
});     