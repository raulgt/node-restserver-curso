const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let rolesValidos = {
    values:['ADMIN_ROLE','USER_ROL'],
    message:'{VALUE} no es un rol valido'
};

let usuarioSchema = new Schema({
    nombre:{
        type: String,
        required: [true, 'El nombre es necesario']
    },
    email:{
        type: String,
        unique: true,
        required: [true, 'El correo es necesario']
    },
    email2:{
        type: String,
        unique: true,
        required: [true, 'El correo secundario es necesario'],
        validate: [function(value){
            return this.email != value;
        }, 'El correo principal debe ser distinto del auxiliar']
    },
    password:{
        type: String,
        required: [true, 'El password es necesario']
    },
    img:{
        type: String,
        required: false
    },
    role:{
        type: String,  
        default: 'USER_ROLE',
        enum: rolesValidos   
    },
    estado: {
        type: Boolean,
        default: true
    },
    google:{
        type: Boolean,
        default: false
    }
});

// Modificamos el schema al momento del envio para evitar mostrar el password
usuarioSchema.methods.toJSON = function(){
      let user = this;
      let userObject = user.toObject();
      delete userObject.password;     

      return userObject;
};

usuarioSchema.plugin(uniqueValidator, {message: 'Error, el valor {VALUE} del campo {PATH} ya se encuentra asignado a otro usuario.'})

module.exports = mongoose.model('Usuario', usuarioSchema);